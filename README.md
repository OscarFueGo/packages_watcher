# packages_watcher

This script monitors changes in installed packages, including new installations, removals, and version changes. It uses an auxiliary file `packages_watcher_aux.json` to record the current state of installed packages.

## How It Works

1. **Initial Setup**: When first executed, the script stores a dictionary of installed packages.
2. **Comparison**: On subsequent runs, it compares the current state of packages with the previously recorded state in `packages_watcher_aux.json`.
3. **Notification**: If there are any changes, it records them in logfile and sends an email with the differences.

## Configuration

The packages tracked are defined by the `REPO` constant at the beginning of the script. Set this constant to the repo name (or a starting string) to track packages in that repo. For example, setting `REPO = "@gem"` will track packages like `@gem-rtsw-epics-base-unstable-2022q4`, `@gem-rtsw-epics-base-unstable`, etc.

## Installation Instructions

1. **Clone Repo**: Clone this repo to `cporpmfs-lv1.cl.gemini.edu` (ADE2) machine, in software's home.
2. **Configure Script**: Adjust constants in the script to match your requirements.
3. **Initial Run**: Execute the script for the first time to create the necessary files and record the installed packages:
    ```sh
    python3 packages_watcher.py
    ```
4. **Automate with Cron**: Set up a cron job to run the script at the desired frequency:
    ```sh
    crontab -e
    ```
    Add the following entry to run the script every minute (adjust the timing as necessary):
    ```sh
    * * * * * /usr/bin/python3 /home/software/packages_watcher/packages_watcher.py > /dev/null 2>&1
    ```

