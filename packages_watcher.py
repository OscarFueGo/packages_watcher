#!/usr/bin/python

#Import libraries
import subprocess
import json
from datetime import datetime
import smtplib
import configparser

#Load Configuration
config = configparser.ConfigParser()
config.read('config.ini')

#Define files path
PATH_AUX=config['PATHS']['PATH_AUX']
PATH_LOGFILE=config['PATHS']['PATH_LOGFILE']

#Repo definition
REPO=config['REPO']['REPO']

#SMTP configuration
SMTP_HOST = config['SMTP']['SMTP_HOST']
FROM = config['SMTP']['FROM']
TO = config['SMTP']['TO']

#Consults DNF for installed packages that include "repo" name, writes auxiliar temporary file 
def load_installed(repo):
    packages_installed={}
    result = subprocess.run(['dnf', 'list', '--installed'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    if result.returncode == 0:
            filtered_lines = [line for line in result.stdout.split('\n') if f'{repo}' in line]
            for line in filtered_lines:
                section = [item for item in line.split(' ') if item]
                package_info={
                    "version": section[1],
                    "repo": section[2]
                }
                packages_installed[section[0]]=package_info
    with open(PATH_AUX, "w") as file:
        file.write(json.dumps(packages_installed, indent=4))
    return packages_installed

#Loads auxiliary file with packages register
def load_latest_register():
    with open(PATH_AUX, "r") as file:
        packages_last_register=json.load(file)
        return packages_last_register

#Checks if file exists, if not, creates it and writes "init_entry" to it
def check_file(FILE_PATH, init_entry):
    try:
        open(FILE_PATH, "r")
    except Exception as e:
        print(e)
        with open(FILE_PATH, "w+") as file:
            file.write(init_entry)
        print(f"File {FILE_PATH} created")

#Sends email with new packages register as body
def send_email(register_log):
    if(register_log!=""):
        try:
            server = smtplib.SMTP(SMTP_HOST)
            MSG = f"Subject: Changes in ADE2 packages\n\n{register_log}"
            server.sendmail(FROM, TO, MSG)
            server.quit()
            print ("Email Sent")
        except Exception as e:
            print(f"Can't send email: {e}")
    else:
        print("No Changes, no email sent")


def main():
    #Check if auxiliray and log files exist, if not, create them
    check_file(PATH_AUX, "{}")
    check_file(PATH_LOGFILE, "")

    #Load package lists (installed and registered)
    packages_last_register = load_latest_register()
    packages_installed = load_installed(REPO)

    #Get datetime and format it
    current_datetime = datetime.now()
    formatted_datetime = current_datetime.strftime("[%m/%d/%y %H:%M]")

    #Get hostname
    hostname = subprocess.check_output(['hostname'], universal_newlines=True).strip()

    register_log=""

    #if lists are equal, nothing to do
    if(packages_installed==packages_last_register):
        print("NO CHANGES!")
    
    #if lists aren't equal
    else:
        #Check if some package was removed
        for pkg_name, pkg_info in packages_last_register.items():
            if(pkg_name not in packages_installed):
                msg= f"{formatted_datetime} ({hostname}) PACKAGE {pkg_name} VERSION {pkg_info['version']} WAS REMOVED"
                print(msg)
                register_log+=msg+"\n"

        #Check if some package has changed version
        for pkg_name, pkg_info in packages_installed.items():
            installed_version = pkg_info['version']
            if(pkg_name in packages_last_register):
                last_registered_version = packages_last_register[pkg_name]['version']
                if (installed_version != last_registered_version):
                    msg = f"{formatted_datetime} ({hostname}) PACKAGE {pkg_name} CHANGED VERSION: {last_registered_version} --> {installed_version}"
                    print(msg)
                    register_log+=msg+"\n"
            #If some package is installed but not registered, then was installed
            else:
                msg=f"{formatted_datetime} ({hostname}) PACKAGE {pkg_name} VERSION {installed_version} WAS INSTALLED"
                print(msg)
                register_log+=msg+"\n"
    #Write logfile
    with open(PATH_LOGFILE, "a+") as file:
        file.write(register_log) 
        file.close()       

    #Send email
    send_email(register_log)
      
if __name__=="__main__":
    main()
